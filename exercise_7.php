<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 7</title>
</head>
<body>
     <div class="container-fluid">
 <div class="login-box">
  <h2>Exercise 7</h2>
  <form method="post">
    <div class="user-box">
      <input type="text" name="word" required="">
      <label>Enter a word: </label>
    </div>
   
    <button type="submit" name ="post"  >
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      Reverse 
    </button> <br><br>
        <h6 style="color: white">
     <?php                    
                        if(isset($_POST['post'])){
                            $string = $_POST['word'];       
                            reverseString($string);
                        }
                        // This function will reverse a word
                        function reverseString($string){
                            $length=strlen($string);
                            if($length %2==0){
                                $half=$length/2;
                                $result=substr($string,$half,$length);
                                echo $result;
                                
                            }else{
                           echo "Reversed string: ".strrev($string);
                            }
                        }
                        
                        // echo "Original Word: ".$string."<br>";
                    ?>
  </form>
</div>
</div>
</body>
</html>