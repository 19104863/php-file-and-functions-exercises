<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Exercise 1</title>
</head>
<body>
<div class="container-fluid">
 <div class="login-box">
  <h2>Exercise 1</h2>
  <form method="post">
    <div class="user-box">
      <input type="text" name="lnumber" required="">
      <label>Enter the line number you want to display: </label>
    </div>
   
    <button type="submit" name ="post"  >
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      Submit 
    </button> <br><br>
        <h6 style="color: white">
        <?php
        // Exercise 1 //
            if (isset($_POST['post'])){
            $lnumber=$_POST['lnumber'];
        function fileLine($filename,$linenumber){
        if(file_exists($filename)){
            $file=file($filename);
            echo $file[$linenumber-1];
            
        }else{
            echo "File doesn't exist!";
        }
        }
        fileLine("testing.text",$lnumber);
            }
        ?>
        </h6>
  </form>
</div>
</div>
   
</body>
</html>