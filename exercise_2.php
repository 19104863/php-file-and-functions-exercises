<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 2</title>
</head>
<body>
    <div class="container-fluid">
 <div class="login-box">
  <h2>Exercise 2</h2>
  <form method="post">
    <div class="user-box">
      <input type="text" name="data" required="">
      <label>Enter the data you want to append: </label>
    </div>
        <div class="user-box">      
      <input type="number" name="lnumber" required="">
      <label> The line where data is added:</label>
    </div>
   
    <button type="submit" name ="post"  >
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      Submit 
    </button> <br><br>
        <h6 style="color: white">
       <?php
            if(isset($_POST['post'])){
                $data=$_POST['data'];
                 $lnumber=$_POST['lnumber'];
            appendLine('testing.text', $data, $lnumber);
            }
            function appendLine($fileName, $data, $lnumber){
            if(file_exists($fileName)){
                $filedata = fopen($fileName, 'a');
                $lines = file ($fileName, FILE_IGNORE_NEW_LINES);
                array_splice($lines, $lnumber-1, 0, $data);
                file_put_contents($fileName, join("\n", ($lines)));
                echo $data. " has been added to your file";
                fclose($filedata);

            }
            }
        
            ?>
        </h6>
  </form>
</div>
</div>
   
</body>
</html>