<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 5</title>
</head>
<body>
    <div class="container-fluid">
 <div class="login-box">
  <h2>Exercise 5</h2>
  <form method="post">
    <div class="user-box">
      <input type="number" name="number" required="">
      <label>Enter a number: </label>
    </div>
   
    <button type="submit" name ="post"  >
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      Calculate 
    </button> <br><br>
        <h6 style="color: white">
        

                  <?php
                      
                        if(isset($_POST['post'])){
                            $input_num = $_POST['number'];
                             numberFactorial($input_num);
                        }
                        // This function will calculate the factorial of the inputted number
                        function numberFactorial($input_num){
                            $factorial=1;
                            if ($input_num <0){
                                   $abs_value=abs($input_num);
                              echo "It's absolute value is ".$abs_value;
                        }else if($input_num >=0){
                                 for ($i = 1; $i <= $input_num; $i++){         
                                 $factorial = $factorial * $i;  
                                    }  
                                echo "The factorial of ".$input_num." is ".$factorial;
                                    }
                        }
                        
                   
                    ?>
  </form>
</div>
</div>
</body>
</html>